FROM ruby:2.6.3
WORKDIR /app
ADD Gemfile Gemfile
ADD Gemfile.lock Gemfile.lock
RUN bundle install
COPY . /app
RUN ["chmod", "u+x", "docker-entrypoint.sh"]
EXPOSE 3000
# README

## Setup
There are two ways of starting the backend, the first one is a traditional rails project local setup and the other one is with Docker.

### Traditional
The use of `rvm` is recommended, but it can be used with a system ruby. The Gemfile will require you to have at least version 2.6.3 to be able to work. 

After the environment is ready:
* Run `gem install bundler` if it is not installed already.
* Run `bundle install` on the root directory to install the dependencies.
* Make sure a Postgres database is running directly on port 5432, this can be either a local install or a Dockerized database.
* Run `rake db:create`
* Run `rake db:migrate`
* Then run the tests with `rake db:test:prepare` and `rake test:db`
* Once the process is completed, run the server with `bin/rails server`, which should start a server in port 3000.

### Dockerized
A `docker-compose.yml` file is available to make the setup easier, it will also prepare a Postgres image with the default port (5432). In case you have a local database running you'll need to change the host port in the file.

After docker is ready and docker-compose is installed (if necessary):
* Run `docker-compose build`
* Run `docker-compose up` or `docker-compose up -d`. The latter will detach itself from the terminal.

The script will then start a server in port 3000.

### Routes

The following routes are available
|URI|Verb|Description|
|---|----|-----------|
|/shortener/get/:url|GET|:url is a shortened url. Returns the stored non-shortened url|
|/shortener/top|GET| Returns a json array with a list of at most 100 urls, ordered by most visited|
|/shortener/create|POST| Takes a `url=` parameter. The url is mapped to a shortened url and the latter is returned.|

### Algorithm

The solution used to get the shortened urls consists of a mapping between the unique ID in the database of a URL and its base-62 equivalent.

The process is more or less the following:

A url is requested -> The url is stored in the database -> The id of the new record is converted to base-62 -> The resulting number is returned as the shortened url

This makes it easy to retrieve the correct url using the base-62 id by converting it back to base 10 and then indexing the database to get the full url.

The reason base 62 was chosen is due to several factors:
* It only comprises numbers (digits), uppercase and lowercase letters.
* Large numbers can be represented with few digits.
* Conversion is fast.
* The use of other bases (like 64) either introduces possible security concerns (javascript and sql can be encoded in base64) or has a lower set of numbers, resulting in a long "shortened" url.

Conversion uses a similar formula to convert to other bases like hexadecimal. A constant is indexed by every digit in its correct position in several modulo operations:

ID 1 -> BASE62[1 % 62] -> 1
ID 12 -> BASE62[12 % 62] -> 12
ID 90 -> BASE62[90 % 62] + BASE62[(90 / 62) % 62] -> R1
etc.
BASE62 is a string of characters, % is the modulo operator, / is floor division and + is concatenation.

### Caveats

Although the base allows for smaller number even to represent 1 million urls stored (in this case with a large ID), this small size won't last forever. That means that after a sizeable amount of urls, the base 62 representation of a new shortened url will be longer than desired.

This could be improved by using multiple tables (for example), with a specific identifier for each one that would be appended to the shortened url, thus limiting the resulting length.

This solution doesn't provide fixed-length shortened urls, so the first 100 urls, for example,
could be encoded in only 2 digit or less, however the redirection logic will always work, regardless of how short or long the shortened url is.





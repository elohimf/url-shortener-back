class UrlShortenerController < ApplicationController
  # Get a shortened url
  def get
    return if params[:url].blank?
    shortener = UrlShortener.find_by(shortened_url: params[:url])
    shortener.increase_visits
    render json: {url: shortener.url}
  end

  # Generate a new shortened url
  def create
    return if params[:url].blank?
    shortener = UrlShortener.find_or_create_by(url: params[:url])
    render json: {shortened_url: shortener.shortened_url}
  end

  # Returns the top 100 most visited shortened urls
  def top
    render json: UrlShortener.top_visits
  end
end

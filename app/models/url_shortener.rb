class UrlShortener < ApplicationRecord
    validates :url, :presence => true, :url => true

    after_create :shorten_url

    def shorten_url
        self.shortened_url = to_base62(self.id)
        self.save
    end

    def increase_visits
        self.visits += 1
        self.save
        self.visits
    end

    def self.top_visits
        UrlShortener.select(:url, :shortened_url, :visits).order(visits: :desc).limit(100).as_json(:except => :id)
    end

    private

    BASE62_CHARACTERS="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"

    def to_base62(num)
        converted = ""
        while num > 0
            remainder = num % 62
            converted.prepend(BASE62_CHARACTERS[remainder])
            num = (num / 62).floor
        end
        converted
    end

    def from_base62(str)
        converted = 0
        str.reverse.each_char.with_index do |c, i| 
            num = BASE62_CHARACTERS.index(c)
            converted += num * (62 ** i)
        end
        converted
    end
end

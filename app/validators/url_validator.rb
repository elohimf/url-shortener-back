class UrlValidator < ActiveModel::EachValidator
    def validate_each(record, attribute, value)
        return if value.blank?
        begin
            HTTParty.head(value).success?
        rescue NoMethodError
            record.errors[attribute] << "#{value} is not a valid url"
            return false
        end
    end
end
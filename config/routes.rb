Rails.application.routes.draw do
  get 'shortener/get/:url', to: "url_shortener#get"
  get 'shortener/get', to: "url_shortener#get"
  post 'shortener/create', to: "url_shortener#create"
  get 'shortener/top', to: "url_shortener#top"
end

class CreateUrlShorteners < ActiveRecord::Migration[6.0]
  def change
    create_table :url_shorteners do |t|
      t.string :url
      t.string :shortened_url
      t.integer :visits, :default => 0

      t.timestamps
    end
  end
end

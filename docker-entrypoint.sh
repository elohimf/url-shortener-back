#!/bin/sh

rm -f /app/tmp/pids/server.pid
rake db:create
rake db:migrate
rake db:test:prepare
rake test:db

rails server -b 0.0.0.0

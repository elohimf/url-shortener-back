require 'test_helper'

class ShortenerControllerTest < ActionDispatch::IntegrationTest
  test "should get get" do
    get shortener_get_url, params: {url: "1"}

    assert_match "{\"url\":\"https://www.google.com\"}", @response.body
    assert_response :success
  end

  test "should post create" do
    # Existing shortened url
    post shortener_create_url, params: {url: "https://www.google.com"}
    assert_match "{\"shortened_url\":\"1\"}", @response.body
    assert_response :success

    # New shortened url
    post shortener_create_url, params: {url: "https://google.com"}
    assert_match "{\"shortened_url\":\"3\"}", @response.body
    assert_response :success
  end

  test "should get top" do
    get shortener_top_url

    assert_match "[{\"url\":\"https://www.facebook.com\","\
    "\"shortened_url\":\"2\",\"visits\":3},{\"url\":\"https://www.google.com\","\
    "\"shortened_url\":\"1\",\"visits\":2}]", @response.body
    assert_response :success
  end

end

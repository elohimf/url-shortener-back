require 'test_helper'

class UrlShortenerTest < ActiveSupport::TestCase
  test "can convert to and from base62" do
    shortener = UrlShortener.new
    random_id = Random.rand(1..5000)

    assert shortener.send(:from_base62, "") == 0
    assert shortener.send(:from_base62, "1S") == 90
    assert shortener.send(:to_base62, 8055) == "25v"
    assert random_id = shortener.send(:from_base62, shortener.send(:to_base62, random_id))
  end

  test "can shorten an url and retrieve it" do
    url_shorteners(:facebook, :google).each do |fixture|
      shortener = UrlShortener.find_or_create_by(url: fixture.url)
      assert shortener.shortened_url == fixture.shortened_url
      assert shortener.url == fixture.url
    end
  end

  test "can increase visits by 1" do
    url_shorteners(:facebook, :google).each do |fixture|
      shortener = UrlShortener.find_or_create_by(shortened_url: fixture.shortened_url)
      visits = shortener.increase_visits
      assert shortener.visits == visits
    end
  end
end
